module Scheme.EvaluationSpec (spec) where

import Test.Hspec
import Repl
import Lisp

processInput :: String -> IO String
processInput input = do
  env <- primitiveBindings
  evalString env input

performTest :: String ->  String -> Expectation
performTest input expected =
  (processInput input) `shouldReturn` expected

spec :: Spec
spec = do
  describe "Literals" $ do
    it "can read integers" $ do
      performTest "17" "17"
    it "can read strings" $ do
      performTest "\"a string\"" "\"a string\""
    it "can read booleans" $ do
      performTest "#t" "#t"
      performTest "#f" "#f"
  describe "syntax is flexible" $ do
    it "is whitespace insensitive" $ do
      performTest "(+ 1 101 )" "102"
  describe "Expressions" $ do
    it "can process arithmetic" $ do
      performTest "(+ 1 101)" "102"
